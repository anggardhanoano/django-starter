from rest_framework import serializers

from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email

from rest_auth.registration.serializers import RegisterSerializer
from rest_auth.serializers import TokenSerializer
from rest_auth.models import TokenModel
from .models import Account


class CustomRegisterSerializer(RegisterSerializer):
    full_name = serializers.CharField(
        required=False,
    )

    def get_cleaned_data(self):
        data_dict = super().get_cleaned_data()
        data_dict['full_name'] = self.validated_data.get('full_name', '')
        return data_dict

class UserDetailsSerializer(serializers.ModelSerializer):
    """
    User model w/o password
    """
    class Meta:
        model = Account
        fields = ('pk', 'username', 'email', 'full_name')
        read_only_fields = ('email', )

class CustomTokenSerializer(serializers.ModelSerializer):

    user = UserDetailsSerializer()

    class Meta:
        model = TokenModel
        fields = ('key','user')

    def get_user(self, obj):
        return obj