from django.db import models
from django.contrib.auth.models import BaseUserManager
from User.models import *


class UserManager(BaseUserManager):

    # jangan diedit jika tidak tau apa yang dilakukan
    # class UserManager mengatur role dari setiap akun

    use_in_migrations = True

    def create_user(self, username, full_name, email, password=None):

        user = self.model(
            username=username,
            full_name=full_name,
            email=email
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, full_name, email, password):
        user = self.create_user(
            username=username,
            password=password,
            full_name=full_name,
            email=email
        )
        user.is_staff = True
        user.save(using=self._db)
        return user
