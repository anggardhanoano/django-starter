from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.template.defaultfilters import slugify
from .etc.UserManager import UserManager


class Account(AbstractBaseUser, PermissionsMixin):

    username = models.CharField(
        max_length=100,
        unique=True
    )
    full_name = models.CharField(
        max_length=100
    )
    email = models.EmailField()
    date_created = models.DateTimeField(
        auto_now=True
    )
    is_staff = models.BooleanField(
        default=False
    )
    is_active = models.BooleanField(
        default=True
    )
    is_verified = models.BooleanField(
        default=False
    )
    slug = models.SlugField(
        default="-"
    )

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['full_name', 'email']

    def __str__(self):
        return self.full_name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.username)
        super(Account, self).save(*args, **kwargs)
        return self.slug

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True
