import factory
from .models import Account

class AccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Account

    username = "test"
    email = "test@test.com"
    full_name = "test"
    