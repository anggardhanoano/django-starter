from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient, force_authenticate
from .factory import AccountFactory

# Create your tests here.
class TestAccount(TestCase):

    def setUp(self):
        self.regis_data = {
            'username' : "jonny",
            'email' : "jonny@test.com",
            'password1' : "test12345",
            'password2' : "test12345",
            'full_name' : "jonny",
        }
        user = AccountFactory()
        user.set_password("test12345")
        user.save()
        self.token = None
        self.user = user
        self.client = APIClient()

    def test_register(self):
        # without request body
        response = self.client.post('/rest-auth/registration/', {})
        self.assertEqual(response.status_code, 400)
        # duplicate username and email
        response = self.client.post('/rest-auth/registration/', {
            'username' : "test",
            'email' : "test@test.com",
            'password1' : "test12345",
            'password2' : "test12345",
            'full_name' : "test",
        })
        self.assertEqual(response.status_code, 400)
        # success story
        response = self.client.post('/rest-auth/registration/', self.regis_data)
        self.assertEqual(response.status_code, 201)

    def test_login(self):
        self.client.login(email=self.user.email, password="test12345")
        # success story
        response = self.client.post('/rest-auth/login/', {
            'username': self.user.username,
            'password': 'test12345'
        })
        self.assertIsNotNone(response.json()["key"])
        self.assertEqual(response.status_code, 200)
        self.token = response.json()["key"]
        # wrong password
        response = self.client.post('/rest-auth/login/', {
            'username': self.user.username,
            'password': 'test'
        })
        self.assertEqual(response.status_code, 400)
        # wrong username
        response = self.client.post('/rest-auth/login/', {
            'username': 'jonny',
            'password': 'test12345'
        })
        self.assertEqual(response.status_code, 400)

    def test_logout(self):
        response = self.client.post('/rest-auth/logout/')
        self.assertEqual(response.status_code, 200)

