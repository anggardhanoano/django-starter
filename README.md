# Django RESR Starter Template + Authentication

#### Template ini dipersiapkan untuk di deploy di heroku

# End Point
  - /rest-auth/registration/
  - /rest-auth/login/
  - /rest-auth/logout/
  - /rest-auth/user/

# Instalasi

- clone repository ini dengan cara
  ```
  git clone https://gitlab.com/anggardhanoano/django-starter.git
  ```
- masuk ke folder django-starter, nama folder dapat bebas diganti setelah di clone
  ```
  cd django-starter
  ```
- inisiasi git di folder yang kamu buat
  ```
  git init
  ```
- buat virtual environment
  ```
  python -m venv env
  ```
  atau
  ```
  python -m virtualenv env
  ```
- aktifkan virtual environment
  - untuk windows
    ```
    env/Scripts/activate.bat
    ```
  - untuk linux/mac
    ```
    source env/bin/activate
    ```
- install semua requirement yang ada, pastikan kamu berada di folder yang sama dengan file requirements.txt
  ```
  pip install -r requirements.txt
  ```
- cek apakah sudah bisa runserver
  ```
  python manage.py runserver
  ```

